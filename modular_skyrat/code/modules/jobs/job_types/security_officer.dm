/datum/job/officer
	title = "Enforcer"
	supervisors = "the Chief Enforcer, and the head of your assigned department (if applicable)"
	department_head = list("Chief Enforcer")
	total_positions = 3
	spawn_positions = 3

/datum/outfit/job/security
	backpack_contents = list(/obj/item/melee/classic_baton/black=1)
	suit_store = /obj/item/gun/energy/e_gun/advtaser
